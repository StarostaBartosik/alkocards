<?php

namespace App\Controller;

use App\Entity\Players;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;



class BaseController extends Controller
{  
   
 
   

 
//    /**
//     * @Route("/", name="start")
//     * @Method({"GET", "POST"})
//     */
//    
//
//    public function newPlayer( Request $request) {
//       
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
//        $session->set('loged', false);
//        
//        $player = new Players();
//        
//        
//        $form = $this->createFormBuilder($player)
//          ->add('Name', TextType::class, array('attr' => array('class' => 'form-control')))
//          ->add('Sex', ChoiceType::class, array(
//            'choices'  => array(
//                'M' => 'M',
//                'K' => 'K',
//                
//            ), 'multiple' => '0',
//              'expanded' => '1',
//              
//            
//          ))
//          ->add('save', SubmitType::class, array(
//            'label' => 'Create',
//            'attr' => array('class' => 'btn btn-primary mt-3')
//          ))
//          ->getForm();
//        $form->handleRequest($request);
//        if($form->isSubmitted() && $form->isValid()) {
//            $player = $form->getData();
//            $player->setDrinks(0);
//
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->persist($player);
//            $entityManager->flush();
//            $playerID = $form->getData()->getId();
//            $session->set('id', $playerID);
//
//            return $this->redirectToRoute('rooms');
//          
//        }
//        return $this->render('new.html.twig', array(
//          'form' => $form->createView()
//        ));
//    }
      
       /**
     * @Route("/", name="start")
     * @Method({"GET", "POST"})
     */
    
    public function newStart(Request $request){
        
        //session_start();
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
        //$session->set('loged', false);
        $_SESSION['loged']=false;
        //$session->set('licznik', $session->get('licznik')+1);
        //var_dump($session->get('licznik'));
        //session_start();
        
        $nick = $request->get('nick');
        $sex = $request->get('sex');
        if( $nick!= null && $sex!= null){
            $player = new Players();
            $player->setName($nick);
            $player->setSex($sex);
            $player->setDrinks(0);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($player);
            $entityManager->flush();
            $playerID = $player->getId();
            $_SESSION['idgracza']=$playerID;
            //$session->set('idgracza', $playerID);
            //$session->set('chuj', "chuj");
            //var_dump($session->get('idgracza'));
            //$_SESSION['idgracza']=$playerID;
            return $this->redirectToRoute('rooms');
            //return new Response();
        }
        else{
            return $this->render('newplayer.html.twig');
        }
    }
    
   
   
}