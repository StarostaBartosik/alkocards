<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomsRepository")
 */
class Rooms
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $RoomName;

   

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Players", mappedBy="WhichRoom")
     */
    private $PlayersID;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Players", cascade={"persist", "remove"})
     */
    private $WhoQueen;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Players", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $admin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $Game;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Players", cascade={"persist", "remove"})
     */
    private $currentPlayer;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $currentCard;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isStarted;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Players", cascade={"persist", "remove"})
     */
    private $previousPlayer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gameEnd;




   

    



    public function __construct()
    {
        $this->whoDrinks = new ArrayCollection();
    }

    

    public function getId()
    {
        return $this->id;
    }

    public function getRoomName(): ?string
    {
        return $this->RoomName;
    }

    public function setRoomName(string $RoomName): self
    {
        $this->RoomName = $RoomName;

        return $this;
    }

 
    

    
    /**
     * @return Collection|Players[]
     */
    public function getPlayersID(): Collection
    {
        return $this->PlayersID;
    }

    public function addPlayersID(Players $playersID): self
    {
        if (!$this->PlayersID->contains($playersID)) {
            $this->PlayersID[] = $playersID;
            $playersID->setWhichRoom($this);
        }

        return $this;
    }

    public function removePlayersID(Players $playersID): self
    {
        if ($this->PlayersID->contains($playersID)) {
            $this->PlayersID->removeElement($playersID);
            // set the owning side to null (unless already changed)
            if ($playersID->getWhichRoom() === $this) {
                $playersID->setWhichRoom(null);
            }
        }

        return $this;
    }

    public function getWhoQueen(): ?Players
    {
        return $this->WhoQueen;
    }

    public function setWhoQueen(?Players $WhoQueen): self
    {
        $this->WhoQueen = $WhoQueen;

        return $this;
    }

    public function getAdmin(): ?Players
    {
        return $this->admin;
    }

    public function setAdmin(Players $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getGame()
    {
        return $this->Game;
    }

    public function setGame($Game): self
    {
        $this->Game = $Game;

        return $this;
    }

    public function getCurrentPlayer(): ?Players
    {
        return $this->currentPlayer;
    }

    public function setCurrentPlayer(?Players $currentPlayer): self
    {
        $this->currentPlayer = $currentPlayer;

        return $this;
    }

    public function getCurrentCard()
    {
        return $this->currentCard;
    }

    public function setCurrentCard($currentCard): self
    {
        $this->currentCard = $currentCard;

        return $this;
    }

    public function getIsStarted(): ?bool
    {
        return $this->isStarted;
    }

    public function setIsStarted(bool $isStarted): self
    {
        $this->isStarted = $isStarted;

        return $this;
    }

    public function getPreviousPlayer(): ?Players
    {
        return $this->previousPlayer;
    }

    public function setPreviousPlayer(?Players $previousPlayer): self
    {
        $this->previousPlayer = $previousPlayer;

        return $this;
    }

    public function getGameEnd(): ?bool
    {
        return $this->gameEnd;
    }

    public function setGameEnd(?bool $gameEnd): self
    {
        $this->gameEnd = $gameEnd;

        return $this;
    }




    
  




    }

