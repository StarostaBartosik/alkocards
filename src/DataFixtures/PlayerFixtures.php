<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PlayerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $player = new \App\Repository\PlayersRepository();
        $player->setName("Ja");
        $player->setSex("M");
        $player->setDrinks(0);
        $manager->persist($player);
        $manager->flush();
    }
}
