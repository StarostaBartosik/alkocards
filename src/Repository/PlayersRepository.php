<?php

namespace App\Repository;

use App\Entity\Players;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Players|null find($id, $lockMode = null, $lockVersion = null)
 * @method Players|null findOneBy(array $criteria, array $orderBy = null)
 * @method Players[]    findAll()
 * @method Players[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Players::class);
    }

//    /**
//     * @return Players[] Returns an array of Players objects
//     */
    
     public function findByID($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.WhichRoom = :value')
            ->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            
            ->getQuery()
            ->getResult()
        ;
    }
    public function findBySex($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.Sex = :value')
            ->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findByIdDrinkOrder($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.WhichRoom = :value')
            ->setParameter('value', $value)
            ->orderBy('p.drinks', 'DESC')
            
            ->getQuery()
            ->getResult()
        ;
    }
  
    

    /*
    public function findOneBySomeField($value): ?Players
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
