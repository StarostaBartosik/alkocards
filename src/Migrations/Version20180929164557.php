<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180929164557 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE players (id INT AUTO_INCREMENT NOT NULL, eight_id INT DEFAULT NULL, which_room_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, sex VARCHAR(1) NOT NULL, drinks INT NOT NULL, has_seen TINYINT(1) DEFAULT NULL, drinks_now TINYINT(1) DEFAULT NULL, is_ready TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_264E43A622E95007 (eight_id), INDEX IDX_264E43A655225C5D (which_room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rooms (id INT AUTO_INCREMENT NOT NULL, who_queen_id INT DEFAULT NULL, admin_id INT NOT NULL, current_player_id INT DEFAULT NULL, previous_player_id INT DEFAULT NULL, room_name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, game VARCHAR(255) DEFAULT NULL, current_card VARCHAR(255) DEFAULT NULL, is_started TINYINT(1) NOT NULL, game_end TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_7CA11A96D9AF9C77 (who_queen_id), UNIQUE INDEX UNIQ_7CA11A96642B8210 (admin_id), UNIQUE INDEX UNIQ_7CA11A9642C04473 (current_player_id), UNIQUE INDEX UNIQ_7CA11A969E94DADE (previous_player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A622E95007 FOREIGN KEY (eight_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A655225C5D FOREIGN KEY (which_room_id) REFERENCES rooms (id)');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A96D9AF9C77 FOREIGN KEY (who_queen_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A96642B8210 FOREIGN KEY (admin_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A9642C04473 FOREIGN KEY (current_player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A969E94DADE FOREIGN KEY (previous_player_id) REFERENCES players (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A622E95007');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A96D9AF9C77');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A96642B8210');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A9642C04473');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A969E94DADE');
        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A655225C5D');
        $this->addSql('DROP TABLE players');
        $this->addSql('DROP TABLE rooms');
    }
}
